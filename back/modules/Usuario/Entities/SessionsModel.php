<?php

namespace Modules\Usuario\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property integer $user_id
 * @property string $ip_address
 * @property string $user_agent
 * @property string $payload
 * @property integer $last_activity
 */
class SessionsModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sessions';

    /**
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'ip_address', 'user_agent', 'payload', 'last_activity'];
}
