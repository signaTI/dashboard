<?php

namespace Modules\Usuario\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $email
 * @property string $pwd
 */
class CadUserModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'api_cad_user';
    protected $hidden = ['pwd'];

    /**
     * @var array
     */
    protected $fillable = ['email'];
}
