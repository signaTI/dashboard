<?php namespace Modules\Usuario\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Usuario\Entities\CadUserModel;
use Modules\Usuario\Entities\SessionsModel;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller {
	
	public function index(Request $request)
	{
		$arr = $request->all();

		if(!isset($arr['email']) || !isset($arr['pwd']))
			return response(json_encode(['message' => 'Não autorizado']), 401);

		$arr['pwd'] = md5($arr['pwd']);

		$objLogin = new CadUserModel();
		$objLogin = CadUserModel::where(['pwd' => $arr['pwd'], 'email' => $arr['email']])->first();

		if($objLogin == null)
			return response(json_encode(['message' => 'Não autorizado']), 401);
		else{
			$this->logar($arr);
			return response(json_encode(['message' => $objLogin, 'session_id' => session()->getId()]), 200);
		}
	}

	private function logar($arr)
	{
		//Pegando ID de usuario
		$objLogin = new CadUserModel();
		$objUsuario = $objLogin->where(['pwd' => $arr['pwd'], 'email' => $arr['email']])->first();

		session()->set('usuario', $objUsuario->toArray());
		session()->save();
	}

	public function logout(Request $request)
	{
		$arr = $request->all();

		if(isset($arr['session_id']) == false)
			return;

		SessionsModel::destroy($arr['session_id']);

		//session()->forget('usuario');
		//session()->save();
	}
}