<?php

Route::group(['prefix' => 'usuario', 'namespace' => 'Modules\Usuario\Http\Controllers'], function()
{
	Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'login'], function()
	{
		// Route::resource('/', 'LoginController');
		Route::get('logout', 'LoginController@logout');
		Route::post('login', 'LoginController@index');
	});

	Route::group(['middleware' => 'auth'], function()
	{
		Route::resource('/', 'UsuarioController');
	});
});