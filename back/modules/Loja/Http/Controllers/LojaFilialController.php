<?php

namespace Modules\Loja\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Loja\Entities\LojaFilialModel;

class LojaFilialController extends Controller {
	
	public function index()
	{
		//Pegando todas as lojas.
		echo LojaFilialModel::all()->toJson();
	}
}