<?php

namespace Modules\Loja\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Loja\Entities\LojaModel;

class LojaController extends Controller {
	
	public function index()
	{
		//Pegando todas as lojas.
		echo LojaModel::all()->toJson();
	}
}