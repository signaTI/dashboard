<?php

Route::group(['middleware' => ['web', 'auth:true'], 'prefix' => 'loja', 'namespace' => 'Modules\Loja\Http\Controllers'], function()
{
	Route::resource('/', 'LojaController');

	Route::group(['prefix' => 'filial'], function()
	{
		Route::resource('/', 'LojaFilialController');
		Route::get('/', 'LojaFilialController@index');
	});
});