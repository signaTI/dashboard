<?php

namespace Modules\Loja\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $id_loja
 * @property integer $website_id
 * @property string $name
 * @property ApiLoja $apiLoja
 */
class LojaFilialModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'api_loja_filial';

    /**
     * @var array
     */
    protected $fillable = ['id_loja', 'website_id', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apiLoja()
    {   
        return $this->belongsTo('Modules\Loja\Entities\LojaModel', 'id_loja');
    }
}
