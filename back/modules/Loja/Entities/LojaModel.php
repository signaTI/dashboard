<?php

namespace Modules\Loja\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 * @property string $nome
 * @property string $plano
 */
class LojaModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'api_lojas';

    /**
     * @var array
     */
    protected $fillable = ['url', 'created_at', 'updated_at', 'nome', 'plano'];

}
