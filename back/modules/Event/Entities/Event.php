<?php 
namespace Modules\Event\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	protected $table = 'events';

	public $timestamps = false;
	
    protected $fillable = ['store_id', 'event_name', 'username', 'get_parameters', 'happened_at'];




	private function _validateEvent($options) {
		
	}


	public function save(array $options = []) {
		$this->_validateEvent($options);
		return parent::save($options);
	}



}