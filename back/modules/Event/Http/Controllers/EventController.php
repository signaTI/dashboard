<?php 
namespace Modules\Event\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Event\Entities\Event;
use Response;
use Request;

class EventController extends Controller {
	
	public function addEvents() {	
		$_response = array('success' => true, 'message' => 'Events added succesfully');	
		$_hasError = false;
		if (Request::has('events')) {
			$_events = Request::input('events');
			foreach ($_events as $eventData) {
				try {
					
					$_event = new Event($eventData);
					$_event->push();
				}

				catch (Exception $e) {
					$_hasError = true;
				}
			}
		}
		else {
			$_response['message'] = 'No events were sent';
		}
	
		return response()->json($_response, $_hasError ? 500 : 200);
	}

	public function getEvents() {
		$_allFilters = array();

		$_params = Request::all();
	
		if (isset($_params['store_id'])) {
			array_push($_allFilters, $this->_defineFilter('store_id', $_params['store_id']));
		}

		if (isset($_params['username'])){
			array_push($_allFilters, $this->_defineFilter('username', $_params['username']));
		}

		if(isset($_params['from'])) {
			array_push($_allFilters, $this->_defineFilter('happened_at_from', $_params['from']));	
		}

		if(isset($_params['to'])) {
			array_push($_allFilters, $this->_defineFilter('happened_at_to', $_params['to']));	
		}

		$events = $this->_listEvents($_allFilters);

	 	return Response::json(
	 		$events
	 	);
	}


	private function _listEvents ($filters) {
		$_events = new Event();

		$_fullFilter = array();
		foreach ($filters as $filter) {
			switch ($filter['type']) {
				
				case 'happened_at_from':
					if (isset($filter['value'])) {
						array_push($_fullFilter, ['happened_at', '>', $filter['value']]);
					}
				break;
				case 'happened_at_to':
					if (isset($filter['value'])) {
						array_push($_fullFilter, ['happened_at', '<', $filter['value']]);
					}
				break;
				case 'store_id':
					if (isset($filter['value'])){
						array_push($_fullFilter, ['store_id', '=', $filter['value']]);
					}
				break;
				case 'username':
					if (isset($filter['value'])){
						array_push($_fullFilter, ['username', '=', $filter['value']]);
					}
				break;
			}
		}
		$_events = $_events->where($_fullFilter);
		$_events = $_events->get();
		return $_events->toArray();
	}

	private function _defineFilter($key, $value) {
		$filter = array("type" => $key);
		$filter['value'] = $value;
		return $filter;
	}

}