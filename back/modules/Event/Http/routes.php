<?php

Route::group(['middleware' => ['web', 'auth:true'], 'prefix' => 'api', 'namespace' => 'Modules\Event\Http\Controllers'], function()
{	
 // 	//filter by username
 // 	Route::get('event/username/{username?}/', 'EventController@getEvents');
 // 		 ->where(['username' => '[\s\S]+']);
	
	// //filter by store id
	// Route::get('event/store_id/{store_id?}/', 'EventController@getEvents')
	// 	 ->where(['store_id' => '[0-9]+']);		 

 	
 	//filter by usename and id
	// Route::get('event/store_id/{store_id?}/{username?}/', 'EventController@getEvents')
	// 	->where(['store_id' => '[0-9]+'])
	// 	->where(['username' => '[\s\S]+']);

	Route::get('event/', 'EventController@getEvents');


	// Route::get('event/', 'EventController@getEvents')->where(['store_id' => '[0-9]+']);
	
	Route::post('event/', 'EventController@addEvents');
});