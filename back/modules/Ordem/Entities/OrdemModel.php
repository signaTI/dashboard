<?php

namespace Modules\Ordem\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $storeid
 * @property integer $entity_id
 * @property integer $id_store_signashop
 * @property string $state
 * @property string $status
 * @property float $grandtotal
 * @property float $shippingamount
 * @property float $subtotal
 * @property string $incrementid
 * @property float $discountamount
 * @property string $customeremail
 * @property string $customername
 * @property string $createdat
 * @property string $updatedat
 * @property string $removeat
 * @property string $shippingmethod
 * @property string $shippingdescription
 * @property string $method
 * @property string $salesflatorder
 * @property string $SalesFlatOrderPayment
 * @property CoreStore $coreStore
 */
class OrdemModel extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'api_sales_flat_order';

    /**
     * @var array
     */
    protected $fillable = ['storeid', 'entity_id', 'id_store_signashop', 'state', 'status', 'grandtotal', 'shippingamount', 'subtotal', 'incrementid', 'discountamount', 'customeremail', 'customername', 'createdat', 'updatedat', 'removeat', 'shippingmethod', 'shippingdescription', 'method', 'salesflatorder', 'SalesFlatOrderPayment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lojaFilial()
    {
        return $this->belongsTo('Modules\Loja\Entities\LojaFilialModel', 'storeid');
    }

    public function loja()
    {
        return $this->belongsTo('Modules\Loja\Entities\LojaModel', 'id_store_signashop');
    }
}
