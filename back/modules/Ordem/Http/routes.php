<?php

Route::group(['middleware' => ['web', 'auth:true'], /*'prefix' => 'ordem',*/ 'namespace' => 'Modules\Ordem\Http\Controllers'], function()
{
	//Route::get('/', 'OrdemController@index');
	Route::resource('ordem', 'OrdemController');
});