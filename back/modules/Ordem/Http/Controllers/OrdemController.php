<?php

namespace Modules\Ordem\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Modules\Ordem\Entities\OrdemModel;

class OrdemController extends Controller {
	
	public function index()
	{
		$arrOrdens = OrdemModel::where(['removeat' => null])->get();

		foreach ($arrOrdens as $key => $objOrdem)
			$this->adicionarValores($objOrdem);

		echo $arrOrdens->toJson();
	}

	public function show($id)
	{
		$objOrdem = OrdemModel::find($id);
		echo $this->adicionarValores($objOrdem)->toJson();
	}

	private function adicionarValores(&$objOrdem)
	{
		$objOrdem->lojaFilial = $objOrdem->lojaFilial()->getResults()->toArray();
		$objOrdem->loja = $objOrdem->loja()->getResults()->toArray();
		return $objOrdem;
	}
}