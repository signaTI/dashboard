<?php

namespace App\Http\Middleware;

use Closure;
use Modules\Usuario\Entities\SessionsModel;
use Illuminate\Support\Facades\Auth;
use Session;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    //public function handle($request, Closure $next, $guard = null, $role = [])
    public function handle($request, Closure $next, $boolExigeAuth = false)
    {
        if($boolExigeAuth == true)
        {
            $_sessionId = $request->header('session-id', null);

            if(empty($_sessionId) == true || SessionsModel::find($_sessionId) == null)
                return response('Não autorizado.', 401);
        }

        /*if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }*/

        return $next($request);
    }
}
