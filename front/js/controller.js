var app = angular.module('mainApp', ['ngRoute', 'ngCookies']);
var URL_ROOT = 'http://backendsignativa.com.br/back/public/';

app.config(function($routeProvider, $httpProvider)
{
	$routeProvider
		.when('/usuarios/login', {
			controller: 'loginController',
			templateUrl: 'login/index.html'
		})
		.when('/dashboard', { 
			controller: 'dashBoardController',
			templateUrl: 'layouts/admin1.html'
		})
		.when('/ordens', { 
			controller: 'ordemController',
			templateUrl: 'layouts/admin1.html'
		})
		.when('/dashboard/usuarios/ordens', {
			controller: 'dashBoardController',
			templateUrl: 'layouts/admin1.html'
		})
		.otherwise({
			redirectTo: '/usuarios/login'
		});

    $httpProvider.interceptors.push(function ($q, $cookies, $location)
    {
        return {
            'request': function (config) {
                config.url = config.url + '?session_id='+$cookies.get('session_id');
                return config;
            },
            'response': function (config)
            {
                return config;
            },
            'responseError': function(config)
            {
            	if($location.url() != '/usuarios/login')
            	{
            		$location.path('usuarios/login');
            	}

            	return config;
            }
        }
    });
})

.run(function ($rootScope, $location, $cookies)
{
  var permissions = [
  	['/', 1],
  	['/dashboard', 0]
  ];

  var checkRoute = function (route)
  {
  	var log = null;

	angular.forEach(permissions, function(value, key)
	{
	  if(value[0] == route){
	  	log = value;
	  	return;
	  }
	});

	return log == null ? false : log;
  };

  $rootScope.$on('$routeChangeStart', function (event, next, current){
    var permissaoUsuario = 0;
    var procura = checkRoute($location.url());

    if(procura != false && procura[1] != 0 && permissaoUsuario < procura[1])
      $location.path('usuarios/login');
  });
});