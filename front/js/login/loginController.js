//var app = angular.module('myApp', []);

app.controller('loginController', function($scope, $http, $location, $cookies)
{
	$scope.login = '';
	$scope.senha = '';

	$scope.fnLogin = function()
	{
		$http({
		  method: 'GET',
		  dataType: 'json',
		  data: 'json',
		  params: {email: $scope.login, pwd: $scope.senha},
		  url: URL_ROOT+'usuario/login'
		}).then(function successCallback(response)
		{
			$cookies.putObject('usuario', response.data.message);
			$cookies.put('session_id', response.data.session_id);

			if(response.status == 200)
				$location.path( "dashboard" );
			else
				SignaMessages.enviar('aviso', response.data.message, 'Autenticação');

		  }, function errorCallback(response)
		  {
		  	SignaMessages.enviar('aviso', response.data.message, 'Autenticação');
		  });
    }
});