//var app = angular.module('myApp', []);

app.controller('ordemController', function($scope, $http)
{
  $scope.arrOrdem = null;

  $scope.ordens = function()
  {
  	$scope.fnOrdens();
  	$scope.template = "ordem/listaOrdem.html";
  }

  $scope.ajustarDadosParaTabela = function (arrItens)
  {
  	var arrFinal = [];

  	for(var i = 0; i < arrItens.length; i += 1)
  	{
  		var objItem = arrItens[i];

  		var arrTmp = [];
  		arrTmp.push(objItem.status);
		arrTmp.push(objItem.incrementid);
		arrTmp.push(objItem.loja.nome);
		arrTmp.push(objItem.lojaFilial.name);
		arrTmp.push(objItem.customeremail);
		arrTmp.push(objItem.createdat);
		arrTmp.push(objItem.updatedat);
		arrTmp.push(objItem.shippingdescription);
		arrTmp.push(objItem.subtotal);

		arrTmp.push("<span onclick='angular.element(j(this)).scope().fnModal("+objItem.id+");'>Mais informações</span>");
  		arrFinal.push(arrTmp);
  	}

  	return arrFinal;
  }

  $scope.fnOrdens = function()
  {
		$http({
		  method: 'GET',
		  dataType: 'json',
		  data: 'json',
		  url: URL_ROOT+'ordem'
		}).then(function successCallback(response)
		{
			if(response.status == 200){
				$scope.arrOrdem = response.data;
				iniciarTabela('#dt2', $scope.ajustarDadosParaTabela($scope.arrOrdem));
			}

		}, function errorCallback(response)
		{
		  	
		});
  }

  $scope.fnGetOrdem = function(id)
  {
  		$http({
		  method: 'GET',
		  dataType: 'json',
		  data: 'json',
		  url: URL_ROOT+'ordem/'+id
		}).then(function successCallback(response)
		{
			if(response.status == 200){
				$scope.objModal = response.data;
			}

		  }, function errorCallback(response)
		  {
		  	
		  });
  }

  $scope.fnModal = function(id)
  {
  	$scope.fnGetOrdem(id);
  	Modal('show');
  }

  if(typeof $scope.template == 'undefined'){
  	$scope.ordens();
  }
});