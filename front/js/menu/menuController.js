app.controller('menuController', function($scope, $location, $http, $cookies)
{
	$scope.usuario = $cookies.getObject('usuario');
	$scope.data = new Date();

	$scope.fnTrocarPagina = function(strPagina)
	{
		$location.path( "/" + strPagina );
		//$scope.template = strPagina;
	}

	$scope.fnLogout = function()
	{
		$http({
		  method: 'GET',
		  dataType: 'json',
		  data: 'json',
		  url: URL_ROOT+'usuario/login/logout'
		}).then(function successCallback(response)
		{
			if(response.status == 200)
				$scope.fnTrocarPagina('usuarios/login');

		}, function errorCallback(response)
		{
		  	
		});
	}
});