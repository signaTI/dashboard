//var app = angular.module('myApp', []);

app.controller('indexController', function($scope, usuarioModel, $http)
{
    usuarioModel.nome = 'Fulano';
    $scope.usuario = usuarioModel;
    $scope.lstDeploys = [];

    $scope.branchEnviar = "";
    $scope.branchEnviarDesabilitado = false;

    $scope.branchEnviarKeyPress = function(e){
    	if(e.charCode == 13){
    		enviarBranch($scope.branchEnviar);
    	}
    }

    var getDeploys = function(){
		$http({
		  method: 'GET',
		  url: 'http://localhost:8080/gignomi/public/bitbucket/api/deploy'
		}).then(function successCallback(response)
		{
			$scope.lstDeploys = response.data;
			$scope.branchEnviarDesabilitado = false;
			//$scope.branchEnviarDesabilitado = response.data.length > 0;
		  }, function errorCallback(response) {
		    
		  });
    }

    setInterval(function(){getDeploys();}, 2000);

    $scope.liberarBranch = function(id){
    	$scope.branchEnviarDesabilitado = true;

    	$http({
		  method: 'GET',
		  url: 'http://localhost:8080/gignomi/public/bitbucket/api/deploy/'+id+'/edit'
		}).then(function successCallback(response)
		{
			getDeploys();
		  }, function errorCallback(response) {
		    
		  });
    }

	var enviarBranch = function(branchEnviar){
		$scope.branchEnviarDesabilitado = true;

		$http({
		  method: 'POST',
		  url: 'http://localhost:8080/gignomi/public/bitbucket/api/deploy',
		  data: {branch: branchEnviar}
		}).then(function successCallback(response)
		{
			getDeploys();
		  }, function errorCallback(response) {
		  });
	}
});