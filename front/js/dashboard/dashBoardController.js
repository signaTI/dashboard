//var app = angular.module('myApp', []);

app.controller('dashBoardController', function($scope, $http)
{
  $scope.arrLoja = null;

  $scope.fnLoja = function()
  {
  	$scope.fnLojas();
  	$scope.template = "dashboard/listaLojas.html";
  }

  $scope.fnLojas = function()
  {
		$http({
		  method: 'GET',
		  dataType: 'json',
		  data: 'json',
		  url: URL_ROOT+'loja'
		}).then(function successCallback(response)
		{
			if(response.status == 200){
				$scope.arrLoja = response.data;
				$scope.fnNormalizarDadosTabela($scope.arrLoja);
			}
		  }, function errorCallback(response)
		  {
		  	console.log(response);
		  });
  }

  $scope.fnNormalizarDadosTabela = function(arrItens)
  {
  	var arrFinal = [];

  	for(var i = 0; i < arrItens.length; i += 1)
  	{
  		var objItem = arrItens[i];

  		var arrTmp = [];
  		arrTmp.push(objItem.url);
		arrTmp.push(objItem.created_at);
		arrTmp.push(objItem.nome);
		arrTmp.push(objItem.plano);

  		arrFinal.push(arrTmp);
  	}

  	iniciarTabela('#dt2', arrFinal);
  }

  if(typeof $scope.template == 'undefined')
  	$scope.fnLoja();
  
});