class SignaMessages
{
	static enviar(strAcao, strMensagem, strTitulo)
	{
		toastr.options.progressBar = true;
		toastr.options.showDuration = 1000;
		toastr.options.hideDuration = 1000;
		toastr.options.timeOut = 7000;

		//toastr.options.extendedTimeOut = parseInt($('select[name="extendedTimeOut"]').val());
		//toastr.options.closeButton = ($('select[name="closeButton"]').val() === 'true');
		//toastr.options.debug = ($('select[name="debug"]').val() === 'true');
		//toastr.options.positionClass = $('select[name="positionClass"]').val();

		//toastr.options.showEasing = "$('select[name="showEasing"]').val()";
		//toastr.options.hideEasing = $('select[name="hideEasing"]').val();
		//toastr.options.showMethod = $('select[name="showMethod"]').val();
		//toastr.options.hideMethod = $('select[name="hideMethod"]').val();

		switch(strAcao)
		{
			case 'erro':
				strAcao = 'error';
				break;

			case 'aviso':
				strAcao = 'warning';
				break;

			case 'sucesso':
				strAcao = 'success';
				break;

			default:
				alert('SignaMessages, o tipo de mensagem: ' + strAcao + ' não existe');
				break;
		}

		if(typeof strTitulo == 'undefined')
			strTitulo = '';

		toastr[strAcao](strMensagem, strTitulo);
	}
}